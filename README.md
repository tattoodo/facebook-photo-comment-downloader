# Facebook Photo Comment Downloader #

So you've got a Facebook post with a lot of images in the comments but you're too lazy to download them manually?
Well you've come to the right place. This simple PHP script will download them for you.

### Requirements ###
* PHP 5.4 (with SSL support)

### Get started ###

* Download the script (download.php)
* Change the variables to fit your needs
    * **$token** - API token. Get yours by pressing "Get Access Token" at https://developers.facebook.com/tools/explorer/
    * **$postid** - The ID of the post you want to download photos from. Get it by visiting the post on Facebook and copying the ID from the URL
    * **$commentlimit** - How many photos do you want to download, max? Set it to the amount of comments or more, to download all.
* Run the script

### Running the script ###
```
$ php downloader.php
```
That's it. It'll download the images and save them in the same folder.
Files will be saved in the format: <number of likes>-<facebook filename>.jpg