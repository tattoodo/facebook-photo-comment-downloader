<?php
/*
 * Script for downloading all uploaded comment photos from Facebook post (Aka. Download all photo comments from a thread)
 * TODO: Use comment ID as the attachment filename so subsequent script runs can check if file exists without fetching attachment info from API
 * Author: Seph Soliman for Tattoodo ApS
 * Thank to Casper Thede Olsen for helping me out with the API calls
 * Note: By prepending like count to filename you may get duplicate images if you run the script several times (since people may be liking the photos inbetween)
 */
$token = 'xxx';
$postid = 613635805410754;
$commentlimit = 5000;

$count = 0;

echo "Fetching comments (max ".$commentlimit.")...\n";
$postraw = file_get_contents('https://graph.facebook.com/'.$postid.'/?fields=comments.fields(attachment,like_count,message).limit('.$commentlimit.')&access_token='.$token);
$post = json_decode($postraw, true);
$comments = $post['comments']['data'];

echo "Got ".count($comments)." comments. Starting download...\n";
foreach($comments as $c) {
	$photo_id = $c['attachment']['target']['id'];
	$comment_url = $c['attachment']['target']['url'];

	if($c['attachment']) {
		$photoraw = file_get_contents('https://graph.facebook.com/'.$photo_id.'/?fields=images.fields(source)&access_token='.$token);
		$photos = json_decode($photoraw, true);
		if($photos === false) {
			echo 'photo fetch failed (images from a comment could not be parsed)';
			break;
		}

		$original = $photos['images'][0]['source']; // We think Facebook lists the original as the first one. Alternatively we can replace "_(n|a).jpg" with "_o.jpg"
		$original_url = parse_url($original);
		$local_name = $c['like_count'].'-'.basename($original_url['path']); // Will download picture as <LIKE COUNT>-<FILENAME>.jpg so we can check them out by popularity

		// Move old file with old like count instead of re-downloading
		$files = glob('*-'.basename($original_url['path']));
		if(count($files) > 0) {
			$old = $files[0];
			if($old != $local_name) {
				echo "Renaming ".$old." to ".$local_name."...\n";
				rename($old, $local_name);
			} else {
				echo "Already got ".$old.". Skipping...\n";
			}
		} else {
			echo "Downloading #".$c['id']." to ".$local_name."...\n";
			file_put_contents($local_name, file_get_contents($original));
			$count++;

			// Attempt to save comment and link to Facebook as file attributes on OS X for easier back-tracking
			@exec("osascript -e 'on run {f, c}' -e 'tell app \"Finder\" to set comment of (POSIX file f as alias) to c' -e end '".$local_name."' '".str_replace(["'", "(", ")"], [' ', " ", " "], $c['message'])."' 2>/dev/null");
			@exec("xattr -w 'com.apple.metadata:kMDItemWhereFroms' '<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\"><array><string>".str_replace("'", "\\'", htmlentities($comment_url))."</string></array></plist>' '".$local_name."'");
		}
	} else {
		echo "Ignoring non-photo comment #".$c['id']."\n";
	}
}

echo "Done. Saved ".$count." photos.\n";
